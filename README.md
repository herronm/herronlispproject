## Maxwell Herron's Lisp Tic-Tac-Toe Game

tic-tac-toe game. To play this game, you will need a lisp environment to run it in.
Once you have your environment set up, copy and paste all the functions into the environment
and press enter. It should now now be ready to run. Simply enter (play-game *brd*) and
the game will begin. It will then prompt you to enter a 0 if you would like to play against
a human, a 1 if you would like to play an AI, or a 2 if you want to see an AI plan against
an AI. Enjoy!

