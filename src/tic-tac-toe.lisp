;; tic-tac-toe.lisp
;;
;; NOTE: FUNCTIONS (print-board, threequal, grab-row, grab-col) AND ASSOCIATED COMMENTS
;; ARE ALL WRITTEN BY ERIK STEINMETZ
;; 
;; Maxwell Herron

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; BEGIN PROGRAM ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Just here as a handy board to test, in general don't use globals
(defparameter *brd* (list '- 'x '- 'o 'o 'o 'x '- '-))   ;; also setf


;; Prints a tic-tac-toe board in a pretty fashion.
;; Param: board - a list containing elements of a ttt board in row-major order
(defun print-board (board)
  (format t "=============")
  (do ((i 0 (+ i 1)) )
      ((= i 9) nil)
      (if (= (mod i 3) 0) 
          (format t "~%|")
          nil)
      (format t " ~A |" (nth i board))
  )
  (format t "~%=============~%")
)


;; Tests whether all three items in a list are equal to each other
;; MAXWELL MADE MODIFICATIONS TO THIS FUNCTION
(defun threequal (list)
  (and (equal (first list) (second list))
       (equal (second list) (third list))
       (not (equal (first list) '-))))



;; Grabs the nth row of a tic-tac-toe board as a list
(defun grab-row (board row)
  (let ( (x (* 3 row)))
    (list (nth x board) (nth (+ 1 x) board) (nth (+ 2 x) board))
  )
)


;; Grabs the nth column of a tic-tac-toe board as a list
(defun grab-col (board col)
  (list (nth col board) (nth (+ 3 col) board) (nth (+ 6 col) board)))
  

;; Gets the number of moves made
(defun get-move-number(board)
    (if (null board)
        0
        (if (eql (car board) '-)
            (+ 0 (get-move-number (cdr board)))
            (+ 1 (get-move-number (cdr board))))))
  
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; BEGIN CODE BY MAXWELL ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  
;; Grabs the forward slash diagonal of the board as a list
(defun grab-forward-slash (board)
    (list (car board) (nth 4 board) (nth 8 board)))


;; Grabs the back slash diagonal of the board as a list
(defun grab-back-slash (board)
	(list (nth 2 board) (nth 4 board) (nth 6 board)))


;; This function places an 'x' on the board. It first checks to make sure that the desired
;; cell is within the range of the board list.
(defun place-x (board cell)
    (if (and (< cell 9) (> cell -1) (not (equal (nth cell board) 'x)) (not (equal (nth cell board) 'o)))
        (progn 
          (setf (nth cell board) 'x)
          (print-board board))
        (progn
          (format t "You chose an illegal move. Please enter a new cell number ~%")
          (place-x board (read)))))


;; This function places an 'o' on the board. It first checks to make sure that the desired
;; cell is within the range of the board list. Once successful, it prints the board.       
(defun place-o (board cell)
    (if (and (< cell 9) (> cell -1) (not (equal (nth cell board) 'o)) (not (equal (nth cell board) 'x)))
        (progn 
          (setf (nth cell board) 'o)
          (print-board board))
        (progn
          (format t "You chose an illegal move. Please enter a new cell number ~%")
          (place-o board (read)))))


;; This function or's together the results of threequal being used on every single row,
;; column, and diagonal to determine if there is a winning configuration.
(defun check-win (board)
    (if (or (threequal(grab-row board 0)) (threequal(grab-row board 1))
     (threequal(grab-row board 2)) (threequal(grab-col board 0))
    (threequal(grab-col board 1)) (threequal(grab-col board 2))
    (threequal(grab-forward-slash board)) (threequal(grab-back-slash board)))
        t
        nil))
 

;; This function simply greets the user and prompts them to answer yes or no to playing
;; the game. If yes, it will call the play-game function, otherwise, program will end.
;; It also checks to see if the user used a valid input (y/n) and if not, it calls lets 
;; them know they gave a bad input and then calls itself again.
(defun prompt-user(board)
	(format t "Welcome to Maxwell's wonderful lisp tic-tac-toe game.
	Would you like to play? (y/n)")
	(handle-yes-no board))


;; This function takes in whichever player it is (x/o) and then calls their respective 
;; function to make a move.
(defun make-move (board player)
    (format t "Choose a cell from 0 - 8~%")
    (if (eql player 'x)
        (place-x board (read))
        (place-o board (read))))

 
;; This function clears the board, setting each slot to a dash -
(defun clear-board (board)
    (if (null board)
        0
        (progn
          (setf (car board) '-)
          (clear-board (cdr board)))))


;; This function simply clears the board and then calls the prompt-user function to
;; welcome the user and see if they would like to play the game.
(defun play-game(board)
	(clear-board board)
	(prompt-game-type board))

 
;; This is the driver function for the game. The game will end if there is a winning
;; configuration, or if all the cells have a move placed within them. The player using
;; 'x' will always go first and the player using 'o' goes second. Upon termination, this
;;  function will ask the player if they would like to play again.
(defun play-game-auxilliary (board)
	(if (or (check-win board) (= 9 (get-move-number board)))
            (play-again-prompt board)
            (progn
              (if (= 0 (mod (get-move-number board) 2))
                  (progn
                    (make-move board 'x)
                    (play-game-auxilliary board))
                  (progn
                    (make-move board 'o)
                    (play-game-auxilliary board))))))
                    

;; This function handles the input reading of whether or not the player wants to play again,
;; and handles any bad inputs associated with this.
(defun play-again-prompt (board)
    (format t "Would you like to play again? (y/n)~%")
    (handle-yes-no board))


;; This function is the input handler for (play-again-prompt). It either ends the program,
;; starts the program over, and calls (play-again-prompt) if a bad input is given.
(defun handle-yes-no (board)
    (let ((value (read)))
          (cond ((equal value 'y) (clear-board board)(play-game board))
                ((equal value 'n) 0)
                ((progn (format t "I'm sorry, that was an invalid input.~%")
                  (play-again-prompt board))))))


;; This function is the input handler for (prompt-game-type). It calls the game type based
;; upon what the user input is, and handles any bad inputs.
(defun handle-game-type-input (board)
    (let ((response (read)))
      (cond ((equal response '0) (play-game-auxilliary board))
            ((equal response '1) (play-against-ai board))
            ((equal response '2) (ai-vs-ai board))
            ((progn (format t "I'm sorry, that was an invalit input.~%")
               (prompt-game-type board))))))


;; This function prompts the user to choose a game type.
(defun prompt-game-type (board)
    (clear-board board)
    (format t "Welcome to Maxwell's wonderful lisp tic-tac-toe game!
    Enter (0) if you would like to play with a friend
    Enter (1) if you would like to play an AI
    Enter (2) if you want to watch an AI play an AI~%")
    (handle-game-type-input board))
	        

;; This function generates a legal move, and then places the move as an x or o, depending
;; on what the human is playing as.
(defun move (board player)
    (let ((cell (generate-legal-move board)))
      (if (eql player 'x)
          (place-o board cell)
          (place-x board cell))))


;; This function generates a move for the AI to choose, and recursively calls itself until
;; the generated move is legal.
(defun generate-legal-move (board)
	(let ((cell (random 9)))
          (if (equal (nth cell board) '-)
              cell
              (generate-legal-move board))))


;; This function is the handler function for (prompt-x-o). It ensures that a legal input is
;; given, and if not, it calls (prompt-x-o) again.
(defun prompt-x-o-handler ()
     (let ((input (read)))
       (cond ((equal input 'x) input)
             ((equal input 'o) input)
             ((progn (format t "I'm sorry, that was an invalid input.~%")
                (prompt-x-o))))))


;; This function prompts the user to whether they want to be x or o.
(defun prompt-x-o ()
    (format t "Would you like to be x or o? (x/o)~%")
    (prompt-x-o-handler))
   
   
;; This function first checks if there is a winning move or if the board is full. It then
;; prompts the player to enter a cell they wish to place their move. After that, the AI
;; places a randomly generated move that is ensured to be legal. It recursively calls itself
;; until the game is done.   
(defun play-against-ai-auxilliary (board player)
    (if (or (check-win board) (= 9 (get-move-number board)))
            (play-again-prompt board)
            (progn
              (if (= 0 (mod (get-move-number board) 2))
                  (progn
                    (make-move board player)
                    (play-against-ai-auxilliary board player))
                  (progn
                    (move board player)
                    (play-against-ai-auxilliary board player))))))
                    

;; This function is the driver function for the player vs the ai. It simply ensures that the
;; board is clear and then calls the auxilliary function that does all the legwork.                   
(defun play-against-ai (board)
    (clear-board board)
    (play-against-ai-auxilliary board (prompt-x-o)))
    

;; This function first checks if there is a winning move or if the board is full. It then
;; has each AI make a randomly generated move that is ensured to be legal. It recursively
;; calls itself until the game is done.
(defun ai-vs-ai-auxilliary (board)
    (if (or (check-win board) (= 9 (get-move-number board)))
            (play-again-prompt board)
            (progn
              (if (= 0 (mod (get-move-number board) 2))
                  (progn
                    (move board 'x)
                    (ai-vs-ai-auxilliary board))
                  (progn
                    (move board 'o)
                    (ai-vs-ai-auxilliary board))))))


;; This function is the driver function for the ai vs the ai. It simply ensures that the 
;; board is clear and then calls the auxilliary function that does all the legwork.
(defun ai-vs-ai (board)
	(clear-board board)
	(ai-vs-ai-auxilliary board))
	
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;; BEGIN TESTING FUNCTIONS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
 
;; This function tests both place move functions by calling each function with an illegal
;; position. If it fails, it will print that it has failed, otherwise, it will print that
;; it has passed the test. 
(defun test-place-move (board)
	(if (or (eql 0 (place-o board 11))
	(eql 0 (place-x board -22)))
            (format t "The place move test has passed ~%")
            (format t "The place move test has failed ~%")))
 

;; Verifies ability to detect a back slash diagonal win by checking a list with a back
;; slash diagonal win.
(defun test-back-slash-win ()
	(if (check-win (list '- '- 'x '- 'x '- 'x '- '-))
        (format t "Winner with back slash verified ~%")
        (format t "Test with back slash failed ~%")))
        

;; Verifies ability to detect a forward slash diagonal win by checking a list with a 
;; forward slash diagonal win
(defun test-forward-slash-win ()
	(if (check-win (list 'x '- '- '- 'x '- '- '- 'x))
        (format t "Winner with forward slash verified ~%")
        (format t "Test with forward slash failed ~%")))


;; Verifies ability to detect a column win by checking a list with with a column win
(defun test-column-win ()
	(if (check-win (list 'x '- '- 'x '- '- 'x '- '-))
        (format t "Winner with column verified ~%")
        (format t "Test with column failed ~%")))


;; Verifies ability to detect a row win by checking a list with a row win
(defun test-row-win () 
	(if (check-win (list 'x 'x 'x '- '- '- '- '- '-))
        (format t "Winner with rows verified ~%")
        (format t "Test with rows failed ~%")))
        
        
;; Verifies that a win will not be detected when no win is present in the list
(defun test-no-win ()
	(if (check-win (list 'x 'o 'o 'x 'o '- '- '- '-))
	(format t "Test with no win has failed ~%")
	(format t "Test with no win has passed ~%")))


;; Master test function that calls all auxiliary test functions   
(defun test-master(board)
	(test-place-move board)
	(test-row-win)
	(test-column-win)
	(test-forward-slash-win)
	(test-back-slash-win))
  
  
  